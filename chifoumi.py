#!/bin/env python3
###########################################
#
# Le chifoumi contre l'ordinateur
#
#
#
###########################################

import random  # pour des choix aléatoires

# choix possibles : pierre, papier, ciseaux
# on associe chaque choix à une valeur numérique
pierre = 0
papier = 1
ciseaux = 2

choix_possibles = [ pierre,
                    papier,
                    ciseaux ]

# nom maximum de victoires pour qu'il y ait un gagnant
nombre_max_victoires = 3

# vos fonctions

def faire_choisir_ordinateur():
    """faire_choisir_ordinateur : On doit faire choisir l'ordinateur
    entre les choix possibles de manière aléatoire (choisir une fonction du module random)
    retourner le choix trouvé"""
    pass

def demander_choix_joueur():
    """demander_choix_joueur : demander au joueur quel est son choix et vérifier
    qu'il fait partie des choix possibles
    retourner le choix du joueur"""
    pass

def afficher_choix(chx_joueur, chx_ordinateur):
    """afficher_choix : afficher le choix du joueur et de l'ordinateur
    sous forme textuelle (pierre, papier, ...).
    ne retourne rien"""
    pass

def comparer_choix(chx_joueur, chx_ordinateur):
    """comparer_choix : Avec une série de conditions, comparer les choix
    et déterminer le gagnant
    retourner le gagnant du tour"""
    pass

def mettre_score_a_jour(gagnant, scr_joueur, scr_ordinateur):
    """mettre_score_a_jour : Mettre les scores à jour en fonction 
    du gagnant du tour.
    retourner si il y a un gagnant pour la partie, le score actualisé du joueur, le score actualisé de l'ordinateur"""
    pass

def afficher_score(scr_joueur, scr_ordinateur):
    """afficher_score : afficher les scores
    ne retourne rien"""
    pass


# le programme principal
tour = 0  # le tour actuel de jeu
score_joueur = 0  # le score du joueur
score_ordinateur = 0  # le score de l'ordinateur
# pas de gagnant == True (Vrai) : il n'y a pas encore de gagnant
# (False : il y a un gagnant)
pas_de_gagnant = True
while pas_de_gagnant :
    choix_joueur = demander_choix_joueur()
    choix_ordinateur = faire_choisir_ordinateur()
    afficher_choix(choix_joueur, choix_ordinateur)
    gagnant_tour = comparer_choix(choix_joueur, choix_ordinateur)
    pas_de_gagnant, score_joueur, score_ordinateur = mettre_score_a_jour(gagnant_tour, score_joueur, score_ordinateur)
    afficher_scores(score_joueur, score_ordinateur)
    tour += 1
gagnant_final = determiner_gagnant(score_joueur, score_ordinateur)
print("Le gagnant est :", gagnant_final, "au bout de ", tour, "tours")
print("Merci d'avoir joué !")

